import React, {Component} from 'react';
import PostCard from './PostCard';
import { Input } from '../../Form/Input';
import { Button } from 'reactstrap';
import qs from 'query-string';
import { Redirect } from 'react-router-dom';
import { api } from '../../../api';

class ListPage extends Component {
  state = {
    posts: [],
    title_like:'',
  };

  componentDidMount() {
    const title_like = qs.parse(this.props.location.search).title_like;
    api.get(`/posts?${qs.stringify({title_like: title_like})}`)
      .then(res => this.setState({ posts: res.data}));
  }

  handleChange = event =>
    this.setState({
      ...this.state,
      [event.target.name]: [event.target.value]
  });

  handleSubmit = event => {
    event.preventDefault();
    return <Redirect to='/form' />
  };
  
  render() {
    return (
      <div>
        <form onSubmit={this.handleChange}>
          <Input name={"title_like"} value={this.state.search} onChange={this.handleChange}/>
          <Button type="submit">Search</Button>
        </form>
        <div className="d-flex align-content-sm-stretch flex-wrap">
          {this.state.posts.map(post => (
            <PostCard post={post} key={post.id} />
          ))}
        </div>
      </div>
    );
  }
}

export default ListPage;
