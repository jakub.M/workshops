import React, {Component} from 'react';
import Form from './Form';
import { api } from '../../../api';

class FormContainer extends Component {
  state = {
    post: {
      title: '',
      body: '',
    },
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    if(id)
    api.get(`posts/${id}`)
    .then(res => this.setState({ ...this.state, post: res.data }))
    .catch(err => console.log(err));
  };

  handleChange = event =>
    this.setState({
      ...this.state,
      post: {
        ...this.state.post,
        [event.target.name]: [event.target.value]
      }
    });

    handleSubmit = event => {
      event.preventDefault();
      api.post('/posts', this.state.post)
        .then(res => console.log(res))
        .catch(err => console.log(err));
    }

  render() {
    return (
      <Form
        post={this.state.post}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default FormContainer;
