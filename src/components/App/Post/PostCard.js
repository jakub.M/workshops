import React from 'react';
import { Link } from 'react-router-dom';
import { Card } from 'reactstrap';

const PostCard = ({ post }) => (
  <div style={{padding: '1rem'}} >
    <Card tag={Link} to={`post/${post.id}`} style={{width: '18rem'}}>
      <img
        className="card-img-top"
        src={post.imageUrl}
        alt={post.title}
      />
      <div className="card-body">
        <p className="card-text">{post.title}</p>
      </div>
    </Card>
  </div>
);

export default PostCard;
