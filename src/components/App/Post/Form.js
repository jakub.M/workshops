import React from 'react'
import PropTypes from 'prop-types';
import { Input } from '../../Form/Input';
import { Textarea } from '../../Form/Textarea';
import { Button } from 'reactstrap';

const Form = ({ post, handleChange, handleSubmit }) => (
  <form style={{padding: '1rem'}} onSubmit={handleSubmit}>
    <Input name={"title"} label="Title" value={post.title} onChange={handleChange} />
    <Textarea name={"body"} lable="Body" value={post.body} onChange={handleChange} />  
    <Button type="submit">Add</Button>
  </form>
);

Form.propType = {
  post: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default Form
