import React, {Component} from 'react';
import axios from 'axios';


class PostView extends Component {
  state = {
    post: {},
  };

  componentDidMount() {
    const api = axios.create({
      baseURL: 'https://react-developers-workshops-server-einjjcgrqx.now.sh',
    });
    api.get(`/posts/${this.props.match.params.id}`)
      .then(res => this.setState({ ...this.state, post: res.data}));
  }
  
  render() {
    const { post } = this.state;
    return (
      <div style={{padding: '1rem', width: '50rem' }}>
        <h1 className="display-6">{post.title}</h1>
        <img
          className="card-img-top"
          src={post.imageUrl}
          alt={post.title}
        />
        <hr className="my-2" />
        <p>{post.body}</p>
      </div>
    );
  }
}

export default PostView;
