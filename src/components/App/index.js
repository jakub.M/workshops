import React, {Component, Fragment} from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import Navbar from './Navbar';
import Main from './Main';
import RouterPage from './RouterPage';
import ListPage from './Post/ListPage';
import FormContainer from './Post/FormContainer';
import NotFound from './NotFound';
import PostViewContainer from './Post/PostView';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Navbar />
          <Main>
            <Switch>
              <Route path={'/posts'} component={ListPage} />
              <Route path={'/post/:id'} component={PostViewContainer} />
              <Route path={'/form/'} component={FormContainer} />
              <Route path={'/edit/:id'} component={FormContainer} />
              <Route path={'/router'} component={RouterPage} />
              <Route exact path="/" component={() => <Redirect to="/posts" />} />
              <Route component={NotFound} />
            </Switch>
          </Main>
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
