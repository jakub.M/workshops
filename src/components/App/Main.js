import React from 'react';

export default ({children}) => (
  <main className="container-fluid" style={{padding: '1rem'}}>{children}</main>
);
