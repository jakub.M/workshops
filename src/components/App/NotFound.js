import React from 'react'
import { Link } from 'react-router-dom';

const NotFound = () => (
  <div>
    <h1 className="display-3">404!</h1>
    <p className="lead">Page not Found</p>
    <hr className="my-2" />
    <p>Correct your path or <Link to='/' >back to site</Link></p>
  </div>
)

export default NotFound
